#!/bin/bash

FILE_PATH=$1
OPERATION=$2
PARAMETER=$3

if [ $# -lt 2 ]; then
  printf "Error:\n Wrong number of parameters.\n"
else
  if [ "$FILE_PATH" -eq "" ]; then
    printf "Error:\n No file name was passed.\n"
  else
    case "$OPERATION" in
      
      ## rename or move file
      "rename"|"move")
        if [ $# -lt 3 ]; then
          printf "Error:\n Missing second parameter.\n"
        else
          mv $FILE_PATH $PARAMETER
        fi
        ;;
      
      ## remove file from disk
      "delete")
        rm $FILE_PATH
        ;;
      
      *)
        printf "Warning:\n Can't resolve operation parameter!\n"
        ;;
    esac
  fi
fi
