#!/bin/bash

## https://community.wia.io/d/15-accessing-the-host-from-inside-a-docker-container
## *#get the IP of the host computer from within Docker container*
## /sbin/ip route|awk '/default/ { print $3 }' 


MOUNT_OR_UMOUNT=$1
TYPE_OF_DISK=$2
LSBLK_NAME=$3

INIC_OUTPUT=1
DEFAULT_LSBLK_NAME="sda"

[ "$LSBLK_NAME" = "" ] && LSBLK_NAME=$DEFAULT_LSBLK_NAME

if [ "$MOUNT_OR_UMOUNT" = "mount" ]; then 
  if [[ "$TYPE_OF_DISK" =~ ^(|all|media)$ ]]; then
    sudo mount /dev/"$LSBLK_NAME"1 /mnt/Media
    if [ $? -eq 0 ]; then  
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Media\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|documents)$ ]]; then
    sudo mount /dev/"$LSBLK_NAME"2 /mnt/Documents
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Documents\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK"  =~ ^(|all|others)$ ]]; then
    sudo cryptsetup luksOpen /dev/"$LSBLK_NAME"3 Others
    sudo mount /dev/mapper/Others /mnt/Others
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Others\n"
    fi
  fi
elif [ "$MOUNT_OR_UMOUNT" = "umount" ]; then  
  if [[ "$TYPE_OF_DISK" =~ ^(|all|media)$ ]]; then
    sudo umount /mnt/Media
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Media\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|documents)$ ]]; then
    sudo umount /mnt/Documents
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Documents\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|others)$ ]]; then
    sudo umount /mnt/Others
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Others\n"
    fi
    sudo cryptsetup luksClose Others
  fi
else
  printf "Warning:\nFirst parameter must be \"mount\" or \"umount\"\n"  
fi
